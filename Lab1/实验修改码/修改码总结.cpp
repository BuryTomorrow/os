thread.h:////////////////////////////////////////////////
	添加： 
void blocked_thread_check(struct thread *t,void *aux UNUSED);
bool thread_cmp_priority(const struct list_elem *a,const struct list_elem *b,void *aux UNUSED);

thread.c/////////////////////////////////////////////////////
	添加： 
void blocked_thread_check(struct thread *t,void *aux UNUSED)
{
  if(t->status == THREAD_BLOCKED && t->ticks_blocked > 0)/*只有线程为阻塞状态以及线程阻塞时间大于0时*/ 
  {
    t->ticks_blocked--;
    if(t->ticks_blocked == 0)
    {
      thread_unblock(t);
    }
  }
}

/* 实现thread_cmp_priority函数*/
bool 
thread_cmp_priority(const struct list_elem *a,const struct list_elem *b,void *aux UNUSED)
{
	return list_entry(a,struct thread,elem)->priority > list_entry(b,struct thread,elem)->priority;
} 

	修改：
void
thread_yield (void) 
{
  struct thread *cur = thread_current ();
  enum intr_level old_level;
  
  ASSERT (!intr_context ());

  old_level = intr_disable ();
  if (cur != idle_thread) 
    list_insert_ordered (&ready_list, &cur->elem,(list_less_func *) &thread_cmp_priority,NULL);
    //修改成插入排序 
  cur->status = THREAD_READY;
  schedule ();
  intr_set_level (old_level);
}

void
thread_unblock (struct thread *t) 
{
  enum intr_level old_level;

  ASSERT (is_thread (t));

  old_level = intr_disable ();
  ASSERT (t->status == THREAD_BLOCKED);
  list_insert_ordered (&ready_list, &t->elem,(list_less_func *) &thread_cmp_priority,NULL);
   //修改成插入排序 
  t->status = THREAD_READY;
  intr_set_level (old_level);
}	
static void
init_thread (struct thread *t, const char *name, int priority)
{
  ASSERT (t != NULL);
  ASSERT (PRI_MIN <= priority && priority <= PRI_MAX);
  ASSERT (name != NULL);

  memset (t, 0, sizeof *t);
  t->status = THREAD_BLOCKED;
  strlcpy (t->name, name, sizeof t->name);
  t->stack = (uint8_t *) t + PGSIZE;
  t->priority = priority;
  t->magic = THREAD_MAGIC;
  list_insert_ordered (&all_list, &t->allelem,(list_less_func *) &thread_cmp_priority,NULL);
  //修改成插入排序 
}

thread_create函数中添加
	t->ticks_blocked = 0;		/*加在关闭中断中避免被中断*/ 


timer.c/////////////////////////////////////////////////////////
void
	修改： 
timer_sleep (int64_t ticks) 
{
  if(ticks <= 0)//避免休眠时间为非正数的时候线程无限阻塞 
  {
  	return ;
  }
  ASSERT (intr_get_level () == INTR_ON);		//断言中断打开 
  enum intr_level old_level = intr_disable();	//关闭中断 
  struct thread *current_thread=thread_current();
  current_thread->ticks_blocked = ticks;		//记录线程需要休眠的时间 
  thread_block();								//阻塞线程 
  intr_set_level(old_level);
}

static void
timer_interrupt (struct intr_frame *args UNUSED)
{
  thread_foreach(blocked_thread_check,NULL);    //中断的时候检测是否有线程需要唤醒 
  ticks++;
  thread_tick ();
}


