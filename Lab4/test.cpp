#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
	//模拟nice值对线程优先级衰减速度的影响 
	for(int nice = 20;nice>=-20;nice--){
		int time=0;
		double priority = 63 ,recent_cpu=0,load_avg=0;
		int ticks=0;
		while(priority > 31){
			ticks++;
			recent_cpu++;
			if(ticks%100==0){
				//load_avg更新，有两个线程在running 
				load_avg=59.0*load_avg/60.0+2.0/60.0;
				//更新recent_cpu
				recent_cpu= (2.0*load_avg)/(2.0*load_avg+1.0)*recent_cpu+nice;
				//计时s
				time++; 
			}
			if(ticks%4==0){
				//更新priority 
				priority=63.0 - recent_cpu/4.0 -nice*2.0; 
				if(priority>63.0)	priority=63.0;
				if(priority<0)		priority=0;
			}
		}
		cout<<"\nthe nice is :"<<nice;
		cout<<"\nwhen priority lower then 31 , cost time : "<<time;
		cout<<"\nthread's priority is :"<<priority<<endl;
	}
	
	return 0;
}

//int main()//测试recent_cpu相关测试 
//{
//	double load_avg=1.0/60.0;
//	double recent_cpu=0;
//	for(int i=0;i<=180;i++){
//		recent_cpu+=100;
//		load_avg=load_avg*59.0/60.0+1.0/60.0;
//		recent_cpu=((2.0*load_avg)/(2.0*load_avg+1.0))*recent_cpu;
//		cout<<fixed<<setprecision(2)<<recent_cpu;
//		if(i%5==0)	cout<<endl;
//		else cout<<"  ";
//	}
//	return 0;
//}
