#ifndef __THREAD_FIXED_POINT_H
#define __THREAD_FIXED_POINT_H

typedef int fixed_t;//后16位为小数，第一位为符号位 
#define FP_SHIFT_AMOUNT 16 //小数部分 
//让整形数变为定点数 
#define FP_CONST(A) ((fixed_t)(A<<FP_SHIFT_AMOUNT))
//加法 
#define FP_ADD(A,B) (A+B)
//减法
#define FP_SUB(A,B) (A-B)
//乘法
#define FP_MULT(A,B) ((fixed_t)(((int64_t) A)*B >> FP_SHIFT_AMOUNT))
//除法 
#define FP_DIV(A,B) ((fixed_t)((((int64_t) A)<<FP_SHIFT_AMOUNT)/B)) 
//获得整数部分
#define FP_INT_PART(A) (A>>FP_SHIFT_AMOUNT)
//获得四舍五入值 
#define FP_ROUND(A) (A >= 0 ? ((A + (1 << (FP_SHIFT_AMOUNT - 1))) >> FP_SHIFT_AMOUNT) \
	    : ((A - (1 << (FP_SHIFT_AMOUNT - 1))) >> FP_SHIFT_AMOUNT)) 
#endif
