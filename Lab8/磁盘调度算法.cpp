#include<iostream>
#include<cmath>
#include<algorithm>
using namespace std;

int ask[1000];
int C0;
int n;
void FCFS(){
	int ans=0;
	int last=C0;
	for(int i=0;i<n;i++){
		cout<<ask[i]<<" ";
		ans+=abs(last-ask[i]);
		last=ask[i];
	}
	cout<<ans<<endl;
}

void SSTF(){
	int last = C0,ans=0;
	bool check[1000]={false};
	for(int i=0;i<n;i++){
		int site = 100,w=-1;
		for(int j=0;j<n;j++){
			if(abs(ask[j]-last)<site&&!check[j]){
				site=abs(ask[j]-last);
				w=j;
			}
			else if(j<w&&abs(ask[j]-last)==site&&!check[j]){
				site=abs(ask[j]-last);
				w=j;
			}
		}
		cout<<ask[w]<<" ";
		check[w]=true;
		ans+=abs(ask[w]-last);
		last=ask[w];
	}
	cout<<ans<<endl;
}

void SCAN(){
	int last = C0,ans=0,w=-1;
	for(int i=0;i<n;i++)
		if(ask[i]<=last) w=i;
	for(int i=w;i>=0;i--){
		ans+=abs(ask[i]-last);
		last=ask[i];
		cout<<ask[i]<<" ";
	}
	if(w<n-1){//回到底部 
		ans+=ask[0]-1;
		last=1;
	}
	for(int i=w+1;i<n;i++){
		ans+=abs(ask[i]-last);
		last=ask[i];
		cout<<ask[i]<<" ";
	}
	cout<<ans<<endl;
}

void CSCAN(){
	int last = C0,ans=0,w=n;
	for(int i=n-1;i>=0;i--)
		if(ask[i]>=last) w=i;
	for(int i=w;i<n;i++){
		ans+=abs(ask[i]-last);
		last=ask[i];
		cout<<ask[i]<<" ";
	}
	if(w<=n&&w>0){//回到底部 
		ans+=99-last;
		last=0;
	}
	for(int i=0;i<w;i++){
		ans+=abs(ask[i]-last);
		last=ask[i];
		cout<<ask[i]<<" ";
	}
	cout<<ans<<endl;
}
int main()
{
	cin>>C0>>n;
	for(int i=0;i<n;i++) cin>>ask[i];
	FCFS();
	SSTF();
	sort(ask,ask+n);
	SCAN();
	CSCAN();
	return 0;
}
