#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>  // define time unit
#include <vector>
#include <fstream>
using namespace std;

//mutex for_thread;

void test(int time)
{
    this_thread::sleep_for(chrono::seconds(time)); // sleep for "time" seconds
    ofstream out("test.txt");
    out << "test" <<endl;
    out.close();
    // for_thread.lock();
    cout << "thread " << this_thread::get_id() << " finish" << endl;
    // for_thread.unlock();
}

int main() {
    cout << "Main thread begin" << endl;
    cout << this_thread::get_id() << endl;
    thread a(test,5);
    thread b(test,0);
    /*
    a.join();
    b.join();
    */
    cout << "Main thread finish" << endl;
    cin.get();
    return 0;
}
