#include <iostream>
#include <thread>
#include <mutex>
#include <ctime>
#include <random>

using namespace std;
int global;
mutex for_global;

void work(int id){	
	while(1){
		if(global>100) return;
		for_global.lock();
		if(global<100&&(global%20>(id-1)*5||global%20<=id*5)){//直到输出100后结束。 
			cout<<"\nthread "<<id<<" printf the number : ";
			for(int i=0;i<5;i++)
				cout<<global++<<" ";
		}
		for_global.unlock();
	}
}

int main()
{
	global = 1;
	cout<<"Main thread begin ";
	vector<thread> threadset;//子线程容器
	for(int i=0;i<4;i++)
		threadset.push_back(thread(work,i+1));//创建4个子线程 
	for(auto&subthread:threadset) 
		subthread.join();
	cout<<"\nMain thread finish \n";
	return 0;
}
