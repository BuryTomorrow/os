#include <iostream>
#include <thread>
#include <mutex>
#include <ctime>
#include <random>

using namespace std;

/************************
Following code is only one kind of implementation template.
Solve the problem in any way you want.
*************************/

void merge_array(int a[], int first, int mid, int last, int temp[])
{
    /************************

        Your Implementation

    ************************/
}

void merge_sort(int array[], int first, int last, int temp[])
{
    /************************

        Your Implementation

    *************************/
}

void merge_start(int array[],int size)
{
    /************************

        Your Implementation

    *************************/
}

void merge_two_thread(int array1[], int size1, int array2[], int size2, int total[])
{
    /************************

        Your Implementation

    *************************/
}

int main()
{
    int n = 100000;
    int* number = new int[n+1];
    for(int i=0;i<n;i++)
    {
        number[i] = rand() % 100000;
    }

    /*************************

        Your Implementation

    *************************/

    delete []number;
    return 0;
}
