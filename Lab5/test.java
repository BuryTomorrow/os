package test;

public class test implements Runnable {
	// public synchronized void run() { 
	public void run() {
		System.out.print("Thread ");
		System.out.print(Thread.currentThread().getId());
		System.out.println(" Finish.");
	}
	
	public static void main(String[] args) throws InterruptedException
	{
		System.out.print("Thread ");
		System.out.print(Thread.currentThread().getId());
		System.out.println(" Begin.");
		
		test testfunc = new test();			// 实例化线程执行函数
		Thread t = new Thread(testfunc);	// 创建线程
		Thread tt = new Thread(testfunc);
		t.start();							// 线程开始执行
		tt.start();
//		t.join();
//		tt.join();
		System.out.print("Thread ");
		System.out.print(Thread.currentThread().getId());
		System.out.println(" Finish.");
	}
}
