#include <iostream>
#include <thread>
#include <mutex>
#include <ctime>
#include <random>

using namespace std;

void merge_array(int a[], int first, int mid, int last, int temp[]){
    int i = first , j = mid +1;
    int m=mid,n=last;
    int k=0;
    while(i<=m&&j<=n){
    	if(a[i]<=a[j])	temp[k++]=a[i++];
    	else	temp[k++]=a[j++];
    }
    while(i<=m)	temp[k++] = a[i++];
    while(j<=n) temp[k++] = a[j++];
    for(i=0;i<k;i++) a[first+i]=temp[i];
}

void merge_sort(int array[], int first, int last, int temp[]){
    if(first<last){
    	int mid = (first+last)/2;
    	merge_sort(array,first,mid,temp);
    	merge_sort(array,mid+1,last,temp);
    	merge_array(array,first,mid,last,temp);
    }
}

void merge_start(int array[],int size){
	int *temp=new int[size];
    merge_sort(array,0,size-1,temp);
    delete []temp;
}

void merge_two_thread(int array1[], int size1, int array2[], int size2, int total[]){
    int i=0,j=0,k=0;
    while(i<size1&&j<size2){
    	if(array1[i]<array2[j]) total[k++]=array1[i++];
    	else total[k++]=array2[j++];
    }
    while(i<size1) total[k++] = array1[i++];
    while(j<size2) total[k++] = array2[j++];
}

bool check(int array[],int n){
	for(int i=1;i<n;i++)
		if(array[i]<array[i-1]) return false;
	return true;
}

int main()
{
    int n = 100000;
    int* number = new int[n+1];
    for(int i=0;i<n;i++)
    {
        number[i] = rand() % 100000;
    }
	
    int size1=n/2;
	int size2=n-size1;
    int *number1 = new int[size1];
    int *number2 = new int[size2];
    for(int i=0;i<size1;i++) number1[i]=number[i];
    for(int i=0;i<size2;i++) number2[i]=number[i+size1];
    
    
	vector<thread> threadset;//子线程容器,创建两个子线程 
	threadset.push_back(thread(merge_start,number1,size1));
	threadset.push_back(thread(merge_start,number2,size2));
//	threadset.push_back(thread(merge_start,number,n));//测试单线程归并排序 

	double start= (float)clock()/CLOCKS_PER_SEC;
	for(auto&subthread:threadset) //让子线程工作 
		subthread.join();

	//合并两个子线程的排序结果 
	merge_two_thread(number1,size1,number2,size2,number);
	
	double end= (float)clock()/CLOCKS_PER_SEC;
    cout<<"总共花费时间: "<<((float)(end-start))<<" s."<<endl;
    if(!check(number,n)) cout<<"排序有错！！\n\n"<<endl; 
    else cout<<"排序正确！！\n\n";
	
	int k = 1000+ rand()% (n/100); 
	int w=1;
	cout<<"部分排序如下：\n";
	for(int i=k-100;i<k;i++){
		cout<<number[i]<<" ";
		if(w++%10==0)cout<<endl;
	}
    delete []number1;
    delete []number2;
    delete []number;
    return 0;
}
