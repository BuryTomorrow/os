import thread
import threading

# lock = thread.allocate_lock()

def test(a):
	# lock.acquire()
	print 'thread 1 begin'
	print a + 1
	print 'thread 1 finish'
	# lock.release()

t = threading.Thread(target=test,args=(2,))
t1 = threading.Thread(target=test,args=(3,))
print 'thread main begin'
t.start()	# thread t starts to run
t1.start()
t.join()	# continues running until thread t ends
t1.join()
print 'thread main finish'

