#include<iostream>
#include<string>
#define MAX 52
using namespace std;
int n;//帧大小 
string s;//输入的访问序列 
struct Frame{//lru,opt模式下需要记录时间 
	string seq;//帧内数据 
	int time[51];//数据的访问时间 
};
struct Result{//字符串0号位为尾，size-1号位为头 
	string fifo;
	int fifo_pf;//fifo模式下的页面错误 
	Frame lru;//lru模式下帧内容记录以及时间记录 
	int lru_pf;//lru模式下的页面错误 
	Frame opt;//opt模式下帧内容记录以及时间记录 
	int opt_pf;//opt模式下的页面错误 
};
Result ans;//记录访问结果 
void FIFO(int A){//fifo模式 
	int t=ans.fifo.find(s[A-1],0);
	if(t==-1){//在帧内找不到则从后插入 
		ans.fifo_pf++;//页面错误增加 
		if(ans.fifo.size()==n)	ans.fifo.erase(0,1);//判断帧是否满，满了则去除尾部 
		ans.fifo+=s[A-1];
	}
}
void LRU(int A){//lru模式 
	int t=ans.lru.seq.find(s[A-1],0);//查找 
	if(t==-1){//不存在帧表中 
		ans.lru_pf++;
		if(ans.lru.seq.size()==n){//如果帧已经满，则找最近最少使用的替换 
			int last_one = 51,k;
			for(int i=0;i<ans.lru.seq.size();i++){
				if(ans.lru.time[i]<last_one){
					last_one = ans.lru.time[i];
					k=i;
				}
			}
			ans.lru.seq[k]=s[A-1];//替换 
			ans.lru.time[k]=A;//更新使用时间 
		}else{//帧表未满，直接压入 
			ans.lru.seq+=s[A-1];
			ans.lru.time[ans.lru.seq.size()-1]=A;
		}
	}else{//如果在帧表中，则直接更新使用时间 
		ans.lru.time[t]=A;
	}
}
void OPT(int A){//opt模式 
	int t=ans.opt.seq.find(s[A-1],0);
	if(t==-1){//不存在表中 
		ans.opt_pf++;
		if(ans.opt.seq.size()==n){
			int a,k,last=-1;//用于记录帧内数据在输入序列第A位后出现的位置 
			for(int i=0;i<ans.opt.seq.size();i++){
				a = s.find_first_of(ans.opt.seq[i],A-1);
				if(a==-1) a=MAX;//如果找不到，则将其位置置为s的最高位还大的位置，如55 
				if(a>last){//找到最晚出现的位置 
					last = a;
					k=i;
				}else if(a==last){//如果有多个最晚出现的位置，则替换最早使用的 
					if(ans.opt.time[i]<ans.opt.time[k]) k=i;
				}
			}
			ans.opt.seq[k]=s[A-1];
			ans.opt.time[k]=A;
		}else{//如果帧表未满，则直接入帧 
			ans.opt.seq+=s[A-1];
			ans.opt.time[ans.opt.seq.size()-1]=A;
		}
	}else{//帧表内找得到，更新使用时间 
		ans.opt.time[t]=A;
	}
}
int main()
{
	int k;
	cin>>n>>s>>k;
	string fifo_seq[MAX],lru_seq[MAX],opt_seq[MAX];//记录每一步操作后的帧表内容 
	int fifo_pf[MAX],lru_pf[MAX],opt_pf[MAX];//记录每一步操作后的页面错误 
	for(int i=1;i<=s.size();i++){//注意要从1开始 
		FIFO(i);	fifo_seq[i]=ans.fifo;	fifo_pf[i]=ans.fifo_pf;//fifo记录 
		LRU(i);		lru_seq[i]=ans.lru.seq;	lru_pf[i]=ans.lru_pf;//lru记录 
		OPT(i);		opt_seq[i]=ans.opt.seq;	opt_pf[i]=ans.opt_pf;//opt记录 
	}
	while(k--){
		int op,A;
		string ins;
		char B;
		cin>>op>>ins>>A;
		switch(op){
			case 1://fifo模式输出 
				if(ins=="seq")
					cout<<fifo_seq[A];
				if(ins=="pf")
					cout<<fifo_pf[A];
				if(ins=="get"){
					cin>>B;
					cout<<((fifo_seq[A].find(B)==-1)? 0:1);
				}
				break;
			case 2://lru模式输出 
				if(ins=="seq")
					cout<<lru_seq[A];
				if(ins=="pf")
					cout<<lru_pf[A];
				if(ins=="get"){
					cin>>B;
					cout<<((lru_seq[A].find(B)==-1)? 0:1);
				}
				break;	
			case 3://opt模式输出 
				if(ins=="seq")
					cout<<opt_seq[A];
				if(ins=="pf")
					cout<<opt_pf[A];
				if(ins=="get"){
					cin>>B;
					cout<<((opt_seq[A].find(B)==-1)? 0:1);
				}
				break;
			default:break;
		}
		cout<<endl;
	}
	return 0;
}
