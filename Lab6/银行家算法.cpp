#include<iostream>

using namespace std;

#define MAX 100
int n,m;//n个进程，m种资源类型 
int available[MAX];//资源类型Rj有k个实例 
int allocation[MAX][MAX];//Pi已分配k个Rj的实例 
int need[MAX][MAX];//Pi还需要k个Rj的实例 
int ans[MAX];//记录结果 

bool safety_algorithm(){//安全算法 
	bool finish[MAX]={false};//判断各个进程是否分配完成 
	int k=0;//记录分配顺序 
	while(1){
		int get = -1;//标记可以分配的进程号 
		for(int i=0;i<n;i++){//找到可以分配的进程 
			if(!finish[i]){
				bool flag = true;
				for(int j=0;j<m;j++)
					if(need[i][j]>available[j]) flag = false;
				if(!flag) continue;//如果i进程不满足，则找下一个进程i+1 
				get = i;
				break;
			}
		}
		//若果存在无法继续分配的情况下还有进程没有被分配资源 
		if(get == -1){
			for(int i=0;i<n;i++)//如果存在未完成的进程，就代表不安全 
				if(!finish[i]) return false;
			break;
		}
		//则直接返回false，表示 not safety 
		//更新available，更新finish,将分配方式存入 
		for(int i=0;i<m;i++)
			available[i]+=allocation[get][i];
		finish[get] = true;
		ans[k++] = get; 
	}
	return true;
}

int main()
{
	int t;
	cin>>t;
	while(t--){
		cin>>n>>m;
		for(int i=0;i<m;i++){//输入各个资源的实例 
			cin>>available[i];
		} 
		for(int i=0;i<n;i++)
			for(int j=0;j<m;j++) 
				cin>>allocation[i][j];//输入进程i分配的 k 个 资源j的实例 
		for(int i=0;i<n;i++)
			for(int j=0;j<m;j++)
				cin>>need[i][j];//输入进程i需要的k个资源j的实例 
				
		if(!safety_algorithm()) cout<<"No";//判断是否安全 
		else {
			cout<<"Yes ";
			for(int i=0;i<n;i++){
				cout<<ans[i];
				if(i!=n-1) cout<<" ";
			}
		}  
		cout<<endl;
	}
	return 0;
} 
