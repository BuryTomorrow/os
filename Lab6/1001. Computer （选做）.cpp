#include<iostream>
#include<queue>
#include<algorithm>
using namespace std;

struct Thread{//线程，有到达时间和剩余工作时间 
	long long release;
	long long process;
};

bool operator <(Thread a,Thread b){//设定优先队列的顺序 
	return a.process >b.process;
}

bool cmp(Thread a,Thread b){//对输入的线程根据到达时间进行排序 
	return a.release <b.release;
}
//剩余最短作业优先 
int main()
{
	int n;
	while(cin>>n){
		priority_queue <Thread> ready;
		Thread thread [n+1];
		for(int i=0;i<n;i++){
			cin>>thread[i].release>>thread[i].process;
		}
		sort(thread,thread+n,cmp);
		long long time = thread[0].release;//从第一个线程开始跑 
		int next = 1;//next作为记录下一个线程的对应数组下标 
		long long ans = 0;//记录结果 
		ready.push(thread[0]);//先将thread[0]放入队列中 
		while(!ready.empty()){ 
			Thread running = ready.top();
			ready.pop();
			if(next==n){//代表所有线程都已经入队
				time+=running.process;
				ans+=time;
			}else{//存在线程还没有到达,也就是还没有开始工作
				//如果当前线程能够在下一个线程到达前完成 
				if(time+running.process<=thread[next].release){
					time+=running.process;
					ans+=time;
				}else{//如果不能，则进行抢占调度，重新调度分配CPU 
					running.process-=thread[next].release-time;
					ready.push(running);
					time = thread[next].release;
				}
			}
			//确保时间能够跳到下一个线程的到达时间（在无ready线程以及存在下一个线程时) 
			if(ready.empty()&&time<thread[next].release&&next<n)
				time = thread[next].release;
			while(next<n&&thread[next].release==time)//将time时刻到达的所有线程入队 
				ready.push(thread[next++]);
		}
		cout<<ans<<endl;
	}
	return 0;
}
